﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebDemo.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebDemo.Controllers.Tests
{
    [TestClass()]
    public class ProductControllerTests
    {
        [TestMethod()]
        public void getstocktotalTest()
        {
            //arrange
            ProductController _ProductController = new ProductController();
            decimal a = 30;
            short b = 5;
            decimal expected = 150;

            //act
            decimal actual = _ProductController.getstocktotal(a, b);
            //assert
            Assert.AreEqual(expected, actual);
            //Assert.Fail();
          
        }
    }
}