﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebDemo.Repository;
using WebDemo.Models;
using System.Data;
using System.Linq.Expressions;
using System.Web.UI.WebControls;
using System.Web.Mvc.Html;
using NLog;

namespace WebDemo.Controllers
{
    public class ProductController : Controller
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private IProductsRepository _productsRepository;
        private ICategoriesRepository _categoriesRepository;
        private ISuppliersRepository _suppliersRepository;

        public ProductController()
        {
            _productsRepository = new ProductsRepository(new Models.NorthwindEntities());
            _categoriesRepository = new CategoriesRepository(new Models.NorthwindEntities());
            _suppliersRepository = new SuppliersRepository(new Models.NorthwindEntities());
        }       

        // GET: Product
        public ActionResult Index()
        {
            var model = _productsRepository.GetAllProducts();
            foreach (var item in model)
            {
                //庫存金額
                item.UnitPrice = getstocktotal((decimal)item.UnitPrice, (short)item.UnitsInStock);
            }
            return View(model);
        }

        public ActionResult Create()
        {
            ViewBag.catagory = _categoriesRepository.GetAllCategories();
            ViewBag.supplier = _suppliersRepository.GetAllSuppliers();
            return View(new Products());
        }
        [HttpPost]
        public ActionResult Create(Products product)
        {
            var result = 0;
            try
            {
                if (ModelState.IsValid)
                {
                    result = _productsRepository.AddProduct(product);

                    if (result==-1)
                    {
                        return RedirectToAction("Index");
                    }
                    
                }
            }
            catch (DataException ex)
            {
                logger.Error(ex.ToString(), "Error");                
            }
            ViewBag.catagory = _categoriesRepository.GetAllCategories();
            ViewBag.supplier = _suppliersRepository.GetAllSuppliers();
            if (result!=0)
            {
                return RedirectToAction("Detail", new { id = result });
            }
            return RedirectToAction("Index");


        }
        public ActionResult Detail(int id)
        {

            Products product = _productsRepository.GetProductById(id);

            ViewBag.supplier = _suppliersRepository.GetSupplierById(product.SupplierID??1);
            ViewBag.catagory = _categoriesRepository.GetCategoryById(product.CategoryID??1);

            return View(product);
        }
        public ActionResult Edit(int id)
        {   

            Products product = _productsRepository.GetProductById(id);          
            
            ViewBag.supplier = getsupplierselect(product.SupplierID);
            ViewBag.catagory = _categoriesRepository.GetAllCategories();

            return View(product);
        }
        [HttpPost]
        public ActionResult Edit(Products product)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _productsRepository.UpdateProduct(product);
                    return RedirectToAction("Detail", new { id = product.ProductID });
                    
                }
            }
            catch (DataException ex)
            {
                logger.Error(ex.ToString(), "Error");               
            }                       

            ViewBag.supplier = getsupplierselect(product.SupplierID);
            ViewBag.catagory = _categoriesRepository.GetAllCategories();
            return View(product);
        }

        public ActionResult Delete(int id)
        {
            _productsRepository.DeleteProduct(id);
            return RedirectToAction("Index");
        }

        public List<SelectListItem> getsupplierselect(int? SupplierID)
        {
            var supplier = _suppliersRepository.GetAllSuppliers();
            var selectList = new List<SelectListItem>();
            foreach (var element in supplier)
            {
                selectList.Add(new SelectListItem
                {
                    Value = element.SupplierID.ToString(),
                    Text = element.CompanyName,
                    Selected = element.SupplierID.Equals(SupplierID),
                });
            }
            return selectList;
        }

        public decimal getstocktotal(decimal unitprice, short stockcount)
        {
            return unitprice * stockcount;
        }

    }
}