﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDemo.Models;

namespace WebDemo.Repository
{
    interface ICategoriesRepository : IDisposable
    {
        IEnumerable<Categories> GetAllCategories();
        Categories GetCategoryById(int categoryId);
        int AddCategory(Categories category);
        int UpdateCategory(Categories category);
        void DeleteCategory(int categoryId);
    }
}
