﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDemo.Models;

namespace WebDemo.Repository
{
    interface IProductsRepository : IDisposable
    {
        IEnumerable<Products> GetAllProducts();
        Products GetProductById(int productId);
        int AddProduct(Products product);
        int UpdateProduct(Products product);
        void DeleteProduct(int ProductId);
    }
}
