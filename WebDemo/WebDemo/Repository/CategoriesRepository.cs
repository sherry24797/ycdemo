﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebDemo.Models;

namespace WebDemo.Repository
{
    public class CategoriesRepository : ICategoriesRepository
    {
        private readonly NorthwindEntities db;

        public CategoriesRepository(NorthwindEntities context)
        {
            db = context;
        }

        public IEnumerable<Categories> GetAllCategories()
        {
            return db.Categories.ToList();
        }

        public Categories GetCategoryById(int categoryId)
        {
            return db.Categories.Find(categoryId);
        }

        public int AddCategory(Categories category)
        {
            int result = -1;

            if (category != null)
            {
                db.Categories.Add(category);
                db.SaveChanges();
                result = category.CategoryID;
            }
            return result;
        }

        public int UpdateCategory(Categories category)
        {
            int result = -1;

            if (category != null)
            {
                db.Entry(category).State = EntityState.Modified;
                db.SaveChanges();
                result = category.CategoryID;
            }
            return result;
        }

        public void DeleteCategory(int categoryId)
        {
            Categories _category = db.Categories.Find(categoryId);
            db.Categories.Remove(_category);
            db.SaveChanges();
        }

        #region IDisposable Support
        private bool disposedValue = false; // 偵測多餘的呼叫  

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: 處置受控狀態 (受控物件)。
                }

                // TODO: 釋放非受控資源 (非受控物件) 並覆寫下方的完成項。
                // TODO: 將大型欄位設為 null。

                disposedValue = true;
            }
        }

        // TODO: 僅當上方的 Dispose(bool disposing) 具有會釋放非受控資源的程式碼時，才覆寫完成項。
        // ~ProductsRepository() {
        //   // 請勿變更這個程式碼。請將清除程式碼放入上方的 Dispose(bool disposing) 中。
        //   Dispose(false);
        // }

        // 加入這個程式碼的目的在正確實作可處置的模式。
        public void Dispose()
        {
            // 請勿變更這個程式碼。請將清除程式碼放入上方的 Dispose(bool disposing) 中。
            Dispose(true);
            // TODO: 如果上方的完成項已被覆寫，即取消下行的註解狀態。
            // GC.SuppressFinalize(this);
        }        
        #endregion

    }
}