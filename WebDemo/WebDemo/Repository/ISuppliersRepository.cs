﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDemo.Models;

namespace WebDemo.Repository
{
    interface ISuppliersRepository : IDisposable
    {
        IEnumerable<Suppliers> GetAllSuppliers();
        Suppliers GetSupplierById(int supplierId);
        int AddSupplier(Suppliers supplier);
        int UpdateSupplier(Suppliers supplier);
        void DeleteSupplier(int supplierId);
    }
}
